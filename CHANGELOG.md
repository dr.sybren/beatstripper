# BeatStripper Changelog

## Version 7.2 - release 2021-11-04

- Support file format 2.0.19, introduced by BeatSaber version 1.18.2

## Version 7.1 - released 2021-09-19

- Always capitalise name picker, even without `-solo` CLI option given.

## Version 7.0 - released 2021-09-19

- Capitalise the player names that are shown in the party mode name picker, after
  finishing a level.
- Support file format 2.0.18, introduced by BeatSaber version 1.17.0.

## Version 6.0 - released 2021-05-08

- Smarter compatibility check of score files. This will make it possible to
  upgrade BeatSaber and keep using the same version of BeatStripper unless they
  actually changed the file format.
- Support file format 2.0.17, introduced by BeatSaber version 1.15.0.

## Version 5.0 - released 2021-03-19

- Show how many solo scores you improved since the last run. Requires running
  with the `-solo` CLI option.
- Skip saving files when there was nothing for BeatStripper to do.
- Support file format 2.0.16, introduced by BeatSaber version 1.14.0.

## Version 4.2 - released 2021-03-09

- Support file format 2.0.15, so support BeatSaber version v1.13.4 and newer.

## Version 4.1 - released 2021-03-05

- Ignore upper/lower case when comparing player names.
- Last version to support BeatSaber file format 2.0.14.

## Version 4.0 - released 2021-03-02

- Experimental support for Linux. If there are issues running on Linux, like
  BeatStripper not finding the right score files, please let me know (see
  README.md).
- Add `-version` CLI argument, which shows the application version and quits BeatStripper.
- Add `-local` CLI argument, which makes the application search for
  `LocalLeaderboards.dat` and `PlayerData.dat` in the current directory. This is
  mostly useful for development & testing.
- Add `-dry-run` CLI argument that runs the entire application but does not save
  any files at the end.
- Fix bug causing double solo highscores to appear in party mode.

## Version 3.1 - released 2021-02-17

- Trim whitespace from around the given player name.
- Ensure that Solo score merging is skipped when an empty player name is given.

## Version 3.0 - released 2021-02-17

- Added ability to merge Solo scores into Party mode.

## Version 2.0 - released 2021-01-24

- Added Text User Interface.

## Version 1.0 - released 2021-01-10

- First working version.
