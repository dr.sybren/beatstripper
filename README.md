# BeatStripper

Edit Party Mode highscores such that each player only retains their highest score.

https://stuvel.eu/software/beatstripper/

## Solo Mode Scores

BeatStripper can optionally add Solo scores to the Party Mode leaderboards. Run
`beatstripper -solo` to enable this.

The first time you run `beatstripper -solo`, it will ask for the solo player's
name. This name will be suffixed with "(solo)", so for example giving a name
"Sybren" would add "Sybren (solo)" to the party mode leaderboards.

*NOTE*: this is still experimental, and only supports installs where there is
only a single Solo player.

## Help, Issues, and Feedback

If you need help, have found an issue, or just want to give feedback, please use
any of these links to contact me:

- [My website](https://stuvel.eu/software/beatstripper/)
- [GitLab](https://gitlab.com/dr.sybren/beatstripper/)
