module gitlab.com/dr.sybren/beatstripper

go 1.13

require (
	github.com/Masterminds/semver v1.5.0
	github.com/charmbracelet/bubbles v0.7.6
	github.com/charmbracelet/bubbletea v0.12.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/muesli/termenv v0.7.4
	github.com/r3labs/diff/v2 v2.13.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.5.1
)
