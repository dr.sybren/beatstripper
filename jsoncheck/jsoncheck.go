package jsoncheck

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"

	"github.com/r3labs/diff/v2"
)

type dataBag map[string]interface{}

// IsCompatible checks whether the parsed data is compatible with the contents of the file.
func IsCompatible(filename string, parsedData interface{}, verbose bool) (isCompatible bool, err error) {
	var parsedDataBag, originalDataBag dataBag
	parsedDataBag, originalDataBag, err = parseBoth(filename, parsedData)
	if err != nil {
		return false, err
	}

	isCompatible = reflect.DeepEqual(originalDataBag, parsedDataBag)
	if !verbose {
		return isCompatible, nil
	}

	if isCompatible {
		fmt.Println("No differences found found.")
		return true, nil
	}

	fmt.Println("Differences found:")
	printDifference(originalDataBag, parsedDataBag)
	return false, nil
}

func checkCompatibility(filename string, parsedData interface{}) (parsedDataBag, originalDataBag dataBag, isCompatible bool, err error) {
	parsedDataBag, originalDataBag, err = parseBoth(filename, parsedData)
	if err != nil {
		return
	}

	isCompatible = reflect.DeepEqual(originalDataBag, parsedDataBag)
	return
}

func parseBoth(filename string, parsedData interface{}) (parsedDataBag, originalDataBag dataBag, err error) {
	// Parse the file without any structs, just as map-of-stuff.
	if originalDataBag, err = parseDataBagFromFile(filename); err != nil {
		return
	}

	// Convert the parsed file back to bytes, and parse as map-of-stuff as well.
	var parsedAsBytes []byte
	if parsedAsBytes, err = json.Marshal(parsedData); err != nil {
		return
	}
	if parsedDataBag, err = parseDataBagFromBytes(parsedAsBytes); err != nil {
		return
	}

	return
}

func parseDataBagFromFile(filename string) (dataBag, error) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	fileContents, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	return parseDataBagFromBytes(fileContents)
}

func parseDataBagFromBytes(fileContents []byte) (dataBag, error) {
	var dataBag dataBag
	if err := json.Unmarshal(fileContents, &dataBag); err != nil {
		return dataBag, err
	}

	return dataBag, nil
}

func printDifference(bag1, bag2 dataBag) {
	changelog, err := diff.Diff(bag2, bag1)
	if err != nil {
		panic(err)
	}

	changesAsJson, err := json.MarshalIndent(changelog, "", "    ")
	if err != nil {
		panic(err)
	}
	os.Stdout.Write(changesAsJson)
	fmt.Println()
}
