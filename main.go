package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"fmt"

	"gitlab.com/dr.sybren/beatstripper/partymode"
	"gitlab.com/dr.sybren/beatstripper/solomode"
	"gitlab.com/dr.sybren/beatstripper/tui"
)

var cliArgs struct {
	options     tui.ProgramOptions
	version     bool
	checkCompat bool
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.options.AddSolo, "solo", false, "Add Solo scores to Party mode leaderboards.")
	flag.BoolVar(&cliArgs.options.UseLocalFiles, "local", false, "Load files from the current directory instead of the game directory.")
	flag.BoolVar(&cliArgs.options.DryRun, "dry-run", false, "Run the process, but do not save the files at the end.")
	flag.BoolVar(&cliArgs.version, "version", false, "Show version number and quit.")
	flag.BoolVar(&cliArgs.checkCompat, "compat", false, "Check compatibility and quit.")
	flag.Parse()
}

func main() {
	parseCliArgs()
	if cliArgs.version {
		fmt.Printf("%s %s\n", tui.ApplicationName, tui.ApplicationVersion)
		return
	}
	if cliArgs.checkCompat {
		checkCompat()
		return
	}

	tui.Main(cliArgs.options)
}

func checkCompat() {
	fmt.Print("Checking solo mode data: ")
	err := solomode.SinglePlayerCheckCompatibility(cliArgs.options.UseLocalFiles, true)
	if err != nil && err != solomode.ErrNotCompatible {
		panic(err)
	}

	fmt.Print("Checking party mode data: ")
	err = partymode.PartyModeCheckCompatibility(cliArgs.options.UseLocalFiles, true)
	if err != nil && err != partymode.ErrNotCompatible {
		panic(err)
	}

}
