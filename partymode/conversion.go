package partymode

import "gitlab.com/dr.sybren/beatstripper/solomode"

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// PlayerScore converts from PlayerData.dat scores to LocalLeaderboards.dat scores.
// i.e. converts a song score score from solo to party mode.
func ConvertPlayerScore(playerName string, lsd solomode.LevelStatsData) PlayerScore {
	return PlayerScore{
		Score:      lsd.HighScore,
		PlayerName: playerName,
		FullCombo:  lsd.FullCombo,
		Timestamp:  0,
	}
}
