package partymode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dr.sybren/beatstripper/solomode"
)

func TestConvertPlayerScore(t *testing.T) {
	lsd := solomode.LevelStatsData{
		LevelID:                   "OneStepCloser",
		Difficulty:                3,
		BeatmapCharacteristicName: "Standard",
		HighScore:                 598673,
		MaxCombo:                  491,
		FullCombo:                 true,
		MaxRank:                   5,
		ValidScore:                true,
		PlayCount:                 11,
	}
	ps := ConvertPlayerScore("Sybren (solo)", lsd)
	expectPS := PlayerScore{
		Score:      598673,
		PlayerName: "Sybren (solo)",
		FullCombo:  true,
		Timestamp:  0,
	}
	assert.EqualValues(t, expectPS, ps)
}
