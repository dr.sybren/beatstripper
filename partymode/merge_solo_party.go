package partymode

import (
	"fmt"

	"gitlab.com/dr.sybren/beatstripper/solomode"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

type AddSoloScoreResult struct {
	LeaderboardID string
	RemovedScore  *PlayerScore
	AddedScore    PlayerScore
}

func (srr AddSoloScoreResult) BeatenPreviousScore() bool {
	if srr.LeaderboardID == "" {
		return false
	}

	if srr.RemovedScore == nil {
		// No previous score, so that's a beat!
		return true
	}

	if srr.AddedScore.Score == srr.RemovedScore.Score {
		return srr.AddedScore.FullCombo && !srr.RemovedScore.FullCombo
	}

	return srr.AddedScore.Score > srr.RemovedScore.Score
}

// AddSoloScore adds a player's Solo score to the Party leaderboard.
// Return the Leaderboard song ID of the modified song, or "" if not found.
func (partyScore *PartyScore) AddSoloScore(soloPlayerData solomode.LocalPlayer, soloSongIndex int, playerNameSoloSuffix string) AddSoloScoreResult {
	soloScores := soloPlayerData.LevelsStatsData
	soloScore := soloScores[soloSongIndex]
	leaderboardID := soloScore.LeaderboardID()

	// Refuse to add zero scores.
	if soloScore.HighScore == 0 {
		return AddSoloScoreResult{}
	}

	playerName := string(soloPlayerData.PlayerName)
	if playerName == "" {
		panic(fmt.Sprintf("unable to add solo score for unnamed player"))
	}
	soloPlayerName := playerName + playerNameSoloSuffix
	scoreToAdd := ConvertPlayerScore(soloPlayerName, soloScore)

	// Find the party score for this song.
	for idx := range partyScore.Leaderboards {
		leaderboard := &partyScore.Leaderboards[idx]
		if leaderboard.LeaderboardID != leaderboardID {
			continue
		}

		removedScore := leaderboard.RemovePlayerScore(soloPlayerName)
		leaderboard.AddScore(scoreToAdd)
		return AddSoloScoreResult{leaderboardID, removedScore, scoreToAdd}
	}

	// Song didn't exist in party mode, so just add it.
	partyScore.AddNewSongScore(leaderboardID, scoreToAdd)
	return AddSoloScoreResult{leaderboardID, nil, scoreToAdd}
}
