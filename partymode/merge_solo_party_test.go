package partymode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dr.sybren/beatstripper/solomode"
)

func mergeTestPartyScore() PartyScore {
	return PartyScore{
		Leaderboards: []Leaderboard{
			{
				LeaderboardID: "NoScoreExpert",
				Scores:        []PlayerScore{},
			},
			{
				LeaderboardID: "TwoScoreNormal",
				Scores: []PlayerScore{
					{
						Score:      161923,
						PlayerName: "Lass",
						FullCombo:  true,
						Timestamp:  1600373081,
					},
					{
						Score:      125650,
						PlayerName: "Dude",
						FullCombo:  false,
						Timestamp:  1600373498,
					},
				},
			},
		},
	}
}

func TestMergeSongNotExistInParty(t *testing.T) {
	partyScore := PartyScore{}
	soloPlayerData := solomode.LocalPlayer{
		PlayerName: "Dude",
		LevelsStatsData: []solomode.LevelStatsData{
			{
				LevelID:                   "OneStepCloser",
				Difficulty:                3,
				BeatmapCharacteristicName: "Standard",
				HighScore:                 598673,
				MaxCombo:                  491,
				FullCombo:                 true,
				MaxRank:                   5,
				ValidScore:                true,
				PlayCount:                 11,
			},
		},
	}
	result := partyScore.AddSoloScore(soloPlayerData, 0, " (solo)")
	assert.Equal(t, "OneStepCloserExpert", result.LeaderboardID)
	assert.Nil(t, result.RemovedScore)
	assert.True(t, result.BeatenPreviousScore())

	expectPartyScore := PartyScore{
		Leaderboards: []Leaderboard{
			{
				LeaderboardID: "OneStepCloserExpert",
				Scores: []PlayerScore{
					{
						Score:      598673,
						PlayerName: "Dude (solo)",
						FullCombo:  true,
						Timestamp:  0,
					},
				},
			},
		},
	}
	assert.EqualValues(t, expectPartyScore, partyScore)
}

func TestMergeDoubleSoloScore(t *testing.T) {
	partyScore := mergeTestPartyScore()
	soloPlayerData := solomode.LocalPlayer{
		PlayerName: "Dude",
		LevelsStatsData: []solomode.LevelStatsData{
			{
				LevelID:                   "TwoScore",
				Difficulty:                solomode.DifficultyNormal,
				BeatmapCharacteristicName: "Standard",
				HighScore:                 145650,
				MaxCombo:                  491,
				FullCombo:                 true,
				MaxRank:                   5,
				ValidScore:                true,
				PlayCount:                 11,
			},
		},
	}

	soloPlayerData.LevelsStatsData[0].HighScore = 1231
	result := partyScore.AddSoloScore(soloPlayerData, 0, " (solo)")
	expectedFirstScore := PlayerScore{
		Score:      1231,
		PlayerName: "Dude (solo)",
		FullCombo:  true,
	}
	assert.Equal(t, "TwoScoreNormal", result.LeaderboardID)
	assert.Equal(t, expectedFirstScore, result.AddedScore)
	assert.True(t, result.BeatenPreviousScore())

	result = partyScore.AddSoloScore(soloPlayerData, 0, " (solo)")
	assert.False(t, result.BeatenPreviousScore())

	soloPlayerData.LevelsStatsData[0].HighScore = 180327
	result = partyScore.AddSoloScore(soloPlayerData, 0, " (solo)")
	assert.Equal(t, "TwoScoreNormal", result.LeaderboardID)
	assert.Equal(t, expectedFirstScore, *result.RemovedScore)
	assert.True(t, result.BeatenPreviousScore())

	expectPartyScore := PartyScore{
		Leaderboards: []Leaderboard{
			{
				LeaderboardID: "NoScoreExpert",
				Scores:        []PlayerScore{},
			},
			{
				LeaderboardID: "TwoScoreNormal",
				Scores: []PlayerScore{
					{
						Score:      180327,
						PlayerName: "Dude (solo)",
						FullCombo:  true,
						Timestamp:  0,
					},
					{
						Score:      161923,
						PlayerName: "Lass",
						FullCombo:  true,
						Timestamp:  1600373081,
					},
					{
						Score:      125650,
						PlayerName: "Dude",
						FullCombo:  false,
						Timestamp:  1600373498,
					},
				},
			},
		},
	}
	assert.EqualValues(t, expectPartyScore, partyScore)
}

func TestMergeCaseInsensitiveSoloScore(t *testing.T) {
	partyScore := mergeTestPartyScore()
	soloPlayerData := solomode.LocalPlayer{
		PlayerName: "Dude",
		LevelsStatsData: []solomode.LevelStatsData{
			{
				LevelID:                   "TwoScore",
				Difficulty:                solomode.DifficultyNormal,
				BeatmapCharacteristicName: "Standard",
				HighScore:                 145650,
				MaxCombo:                  491,
				FullCombo:                 true,
				MaxRank:                   5,
				ValidScore:                true,
				PlayCount:                 11,
			},
		},
	}

	soloPlayerData.LevelsStatsData[0].HighScore = 1231
	result := partyScore.AddSoloScore(soloPlayerData, 0, " (Solo)") // Capitalised "Solo"
	assert.Equal(t, "TwoScoreNormal", result.LeaderboardID)
	assert.Nil(t, result.RemovedScore)
	assert.True(t, result.BeatenPreviousScore())

	soloPlayerData.LevelsStatsData[0].HighScore = 180327
	result = partyScore.AddSoloScore(soloPlayerData, 0, " (solo)") // lower-case "solo"
	assert.Equal(t, "TwoScoreNormal", result.LeaderboardID)
	assert.Equal(t, int64(1231), result.RemovedScore.Score)
	assert.True(t, result.BeatenPreviousScore())

	expectPartyScore := PartyScore{
		Leaderboards: []Leaderboard{
			{
				LeaderboardID: "NoScoreExpert",
				Scores:        []PlayerScore{},
			},
			{
				LeaderboardID: "TwoScoreNormal",
				Scores: []PlayerScore{
					{
						Score:      180327,
						PlayerName: "Dude (solo)",
						FullCombo:  true,
						Timestamp:  0,
					},
					{
						Score:      161923,
						PlayerName: "Lass",
						FullCombo:  true,
						Timestamp:  1600373081,
					},
					{
						Score:      125650,
						PlayerName: "Dude",
						FullCombo:  false,
						Timestamp:  1600373498,
					},
				},
			},
		},
	}
	assert.EqualValues(t, expectPartyScore, partyScore)
}

func TestMergeNoPreviousSoloScore(t *testing.T) {
	partyScore := mergeTestPartyScore()
	soloPlayerData := solomode.LocalPlayer{
		PlayerName: "Dude",
		LevelsStatsData: []solomode.LevelStatsData{
			{
				LevelID:                   "TwoScore",
				Difficulty:                solomode.DifficultyNormal,
				BeatmapCharacteristicName: "Standard",
				HighScore:                 145650,
				MaxCombo:                  491,
				FullCombo:                 true,
				MaxRank:                   5,
				ValidScore:                true,
				PlayCount:                 11,
			},
		},
	}
	result := partyScore.AddSoloScore(soloPlayerData, 0, " (solo)")
	assert.Equal(t, "TwoScoreNormal", result.LeaderboardID)
	assert.Nil(t, result.RemovedScore)

	expectPartyScore := PartyScore{
		Leaderboards: []Leaderboard{
			{
				LeaderboardID: "NoScoreExpert",
				Scores:        []PlayerScore{},
			},
			{
				LeaderboardID: "TwoScoreNormal",
				Scores: []PlayerScore{
					{
						Score:      161923,
						PlayerName: "Lass",
						FullCombo:  true,
						Timestamp:  1600373081,
					},
					{
						Score:      145650,
						PlayerName: "Dude (solo)",
						FullCombo:  true,
						Timestamp:  0,
					},
					{
						Score:      125650,
						PlayerName: "Dude",
						FullCombo:  false,
						Timestamp:  1600373498,
					},
				},
			},
		},
	}
	assert.EqualValues(t, expectPartyScore, partyScore)
}
