package partymode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	"gitlab.com/dr.sybren/beatstripper/jsoncheck"
)

// PartyScore represents a LocalLeaderboards.dat file.
type PartyScore struct {
	Leaderboards []Leaderboard `json:"_leaderboardsData"`
}

// Leaderboard contains the scores for a song at a specific difficulty level.
type Leaderboard struct {
	LeaderboardID string        `json:"_leaderboardId"`
	Scores        []PlayerScore `json:"_scores"`
}

// PlayerScore repesents a single score of a single player.
type PlayerScore struct {
	Score      int64  `json:"_score"`
	PlayerName string `json:"_playerName"`
	FullCombo  bool   `json:"_fullCombo"`
	Timestamp  int64  `json:"_timestamp"`
}

// AddNewSongScore adds a new song + a single score.
// The caller is responsible for ensuring the LeaderboardID is unique.
func (ps *PartyScore) AddNewSongScore(leaderboardID string, playerScore PlayerScore) {
	leaderboard := Leaderboard{
		LeaderboardID: leaderboardID,
		Scores:        []PlayerScore{playerScore},
	}
	ps.Leaderboards = append(ps.Leaderboards, leaderboard)
}

// ByScore implements sort.Interface for []PlayerScore based on the Score field.
type ByScore []PlayerScore

func (a ByScore) Len() int           { return len(a) }
func (a ByScore) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByScore) Less(i, j int) bool { return a[i].Score > a[j].Score }

// SortScores performs an in-place sort of the scores.
func (lb *Leaderboard) SortScores() {
	sort.Sort(ByScore(lb.Scores))
}

// AddScore adds a single score and then sorts the scores.
func (lb *Leaderboard) AddScore(playerScore PlayerScore) {
	lb.Scores = append(lb.Scores, playerScore)
	lb.SortScores()
}

// RemovePlayerScore removes a single PlayerScore from this leaderboard.
// Return the removed score when a score has been removed, nil otherwise.
func (lb *Leaderboard) RemovePlayerScore(playerName string) *PlayerScore {
	playerName = strings.ToLower(playerName)
	for scoreIndex := range lb.Scores {
		score := lb.Scores[scoreIndex]
		if strings.ToLower(score.PlayerName) != playerName {
			continue
		}

		lb.Scores = append(lb.Scores[:scoreIndex], lb.Scores[scoreIndex+1:]...)
		return &score
	}
	return nil
}

// ParseAndVerify parses the score file and verifies its contents can be parsed.
func ParseAndVerify(filename string) (PartyScore, error) {
	partyscore, err := ParsePartyMode(filename)
	if err != nil {
		return PartyScore{}, err
	}

	isCompatible, err := jsoncheck.IsCompatible(filename, partyscore, false)
	if err != nil {
		return PartyScore{}, err
	}
	if !isCompatible {
		return PartyScore{}, ErrNotCompatible
	}

	return partyscore, nil
}

// ParsePartyMode parses a LocalLeaderboards.dat file.
func ParsePartyMode(filename string) (PartyScore, error) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		return PartyScore{}, err
	}
	defer jsonFile.Close()

	fileContents, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return PartyScore{}, err
	}

	var partyscore PartyScore
	if err := json.Unmarshal(fileContents, &partyscore); err != nil {
		return PartyScore{}, err
	}

	return partyscore, nil
}

// SavePartyMode parses a LocalLeaderboards.dat file.
func SavePartyMode(partyscore PartyScore, filename string) error {
	jsonBytes, err := json.Marshal(partyscore)
	if err != nil {
		return err
	}

	jsonFile, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	_, err = jsonFile.Write(jsonBytes)
	if err != nil {
		return err
	}

	return nil
}

// PruneScores removes all the scores except the highest score of each player.
func PruneScores(leaderboard Leaderboard) Leaderboard {
	newBoard := Leaderboard{
		LeaderboardID: leaderboard.LeaderboardID,
		Scores:        []PlayerScore{},
	}

	// Get only the best scores.
	bestScores := map[string]PlayerScore{}
	for _, score := range leaderboard.Scores {
		playerName := strings.ToLower(score.PlayerName)
		bestSoFar, found := bestScores[playerName]
		if !found || score.Score > bestSoFar.Score {
			bestScores[playerName] = score
		}
	}

	// Join them into an array.
	for _, score := range bestScores {
		newBoard.Scores = append(newBoard.Scores, score)
	}

	// Sort by highest score first.
	sort.Slice(newBoard.Scores[:], func(i, j int) bool {
		scoreA := &newBoard.Scores[i]
		scoreB := &newBoard.Scores[j]
		if scoreA.Score == scoreB.Score {
			return scoreA.FullCombo
		}
		return scoreA.Score > scoreB.Score
	})

	// Transform player names to Title Case.
	for index := range newBoard.Scores {
		newBoard.Scores[index].PlayerName = strings.Title(newBoard.Scores[index].PlayerName)
	}

	return newBoard
}
