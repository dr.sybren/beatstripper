package partymode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPruneEmpty(t *testing.T) {
	inputScore := Leaderboard{}
	expectScore := Leaderboard{"", []PlayerScore{}}
	assert.Equal(t, expectScore, PruneScores(inputScore))
}

func TestPruneNoDuplicates(t *testing.T) {
	inputScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      180103,
				PlayerName: "Lass",
				FullCombo:  true,
				Timestamp:  1607113555,
			},
			{
				Score:      161923,
				PlayerName: "Sybren",
				FullCombo:  true,
				Timestamp:  1600373081,
			},
			{
				Score:      125650,
				PlayerName: "Dude",
				FullCombo:  false,
				Timestamp:  1600373498,
			},
		},
	}
	assert.Equal(t, inputScore, PruneScores(inputScore))
}

func TestPruneRandomOrderDuplicates(t *testing.T) {
	inputScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      140103,
				PlayerName: "Sybren",
				FullCombo:  true,
				Timestamp:  20,
			},
			{
				Score:      180103,
				PlayerName: "Sybren",
				FullCombo:  true,
				Timestamp:  10,
			},
			{
				Score:      125650,
				PlayerName: "Sybren",
				FullCombo:  false,
				Timestamp:  30,
			},
		},
	}
	expectScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      180103,
				PlayerName: "Sybren",
				FullCombo:  true,
				Timestamp:  10,
			},
		},
	}

	assert.Equal(t, expectScore, PruneScores(inputScore))
}

func TestPruneSameScoreDifferentCombo(t *testing.T) {
	inputScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      140103,
				PlayerName: "Sybren",
				FullCombo:  false,
				Timestamp:  20,
			},
			{
				Score:      140103,
				PlayerName: "Dude",
				FullCombo:  true,
				Timestamp:  25,
			},
		},
	}
	expectScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      140103,
				PlayerName: "Dude",
				FullCombo:  true,
				Timestamp:  25,
			},
			{
				Score:      140103,
				PlayerName: "Sybren",
				FullCombo:  false,
				Timestamp:  20,
			},
		},
	}

	assert.Equal(t, expectScore, PruneScores(inputScore))
}

func TestPruneSingleSongDuplicates(t *testing.T) {
	inputScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      180103,
				PlayerName: "Lass",
				FullCombo:  true,
				Timestamp:  1607113555,
			},
			{
				Score:      161923,
				PlayerName: "lass", // duplicate with lower case
				FullCombo:  true,
				Timestamp:  1600373081,
			},
			{
				Score:      125650,
				PlayerName: "Dude",
				FullCombo:  false,
				Timestamp:  1600373498,
			},
		},
	}
	expectScore := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{
			{
				Score:      180103,
				PlayerName: "Lass",
				FullCombo:  true,
				Timestamp:  1607113555,
			},
			{
				Score:      125650,
				PlayerName: "Dude",
				FullCombo:  false,
				Timestamp:  1600373498,
			},
		},
	}

	assert.Equal(t, expectScore, PruneScores(inputScore))
}

func TestAddNewSongScore(t *testing.T) {
	inputScore := PartyScore{[]Leaderboard{{
		LeaderboardID: "INeedYouNormal",
		Scores: []PlayerScore{{
			Score:      180103,
			PlayerName: "Lass",
			FullCombo:  true,
			Timestamp:  1607113555,
		}},
	}}}

	newScore := PlayerScore{
		Score:      43145,
		PlayerName: "Dude",
		FullCombo:  false,
		Timestamp:  5164425,
	}

	expectScore := PartyScore{[]Leaderboard{
		{
			LeaderboardID: "INeedYouNormal",
			Scores: []PlayerScore{{
				Score:      180103,
				PlayerName: "Lass",
				FullCombo:  true,
				Timestamp:  1607113555,
			}},
		},
		{
			LeaderboardID: "NewSongExpertPlus",
			Scores:        []PlayerScore{newScore},
		},
	}}

	inputScore.AddNewSongScore("NewSongExpertPlus", newScore)
	assert.Equal(t, expectScore, inputScore)
}

func TestRemovePlayerScore(t *testing.T) {
	scoreLass1 := PlayerScore{
		Score:      180103,
		PlayerName: "Lass",
		FullCombo:  true,
		Timestamp:  1607113555,
	}
	scoreLass2 := PlayerScore{
		Score:      161923,
		PlayerName: "Lass",
		FullCombo:  true,
		Timestamp:  1600373081,
	}
	scoreDude := PlayerScore{
		Score:      125650,
		PlayerName: "Dude",
		FullCombo:  false,
		Timestamp:  1600373498,
	}

	inputBoard := Leaderboard{
		LeaderboardID: "INeedYouNormal",
		Scores:        []PlayerScore{scoreLass1, scoreLass2, scoreDude},
	}

	assert.Equal(t, scoreLass1, *inputBoard.RemovePlayerScore("Lass"))
	assert.EqualValues(t,
		Leaderboard{
			LeaderboardID: "INeedYouNormal",
			Scores:        []PlayerScore{scoreLass2, scoreDude},
		},
		inputBoard)

	assert.Equal(t, scoreLass2, *inputBoard.RemovePlayerScore("Lass"))
	assert.EqualValues(t,
		Leaderboard{
			LeaderboardID: "INeedYouNormal",
			Scores:        []PlayerScore{scoreDude},
		},
		inputBoard)

	assert.Nil(t, inputBoard.RemovePlayerScore("Lass"))
	assert.EqualValues(t,
		Leaderboard{
			LeaderboardID: "INeedYouNormal",
			Scores:        []PlayerScore{scoreDude},
		},
		inputBoard)

	assert.Equal(t, scoreDude, *inputBoard.RemovePlayerScore("Dude"))
	assert.EqualValues(t,
		Leaderboard{
			LeaderboardID: "INeedYouNormal",
			Scores:        []PlayerScore{},
		},
		inputBoard)

	assert.Nil(t, inputBoard.RemovePlayerScore("Dude"))
	assert.EqualValues(t,
		Leaderboard{
			LeaderboardID: "INeedYouNormal",
			Scores:        []PlayerScore{},
		},
		inputBoard)
}
