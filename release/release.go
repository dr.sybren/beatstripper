package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/beatstripper/tui"
)

const gitlabURL = "https://gitlab.com/api/v4/projects/dr.sybren%2Fbeatstripper/releases"

type gitlabRelease struct {
	Name        string `json:"name"`
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
	Assets      struct {
		Links []gitlabLink `json:"links"`
	} `json:"assets"`
}

type gitlabLink struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type anyDict map[string]interface{}

var cliArgs struct {
	version  string
	fileglob string
}

func parseCliArgs() {
	flag.StringVar(&cliArgs.version, "version", "", "Version to release")
	flag.StringVar(&cliArgs.fileglob, "fileglob", "", "Glob of files to include")
	flag.Parse()
}

func makeRequestPayload() []byte {
	logrus.WithFields(logrus.Fields{
		"version":  cliArgs.version,
		"fileglob": cliArgs.fileglob,
	}).Info("constructing GitLab release JSON")

	release := gitlabRelease{
		Name:        cliArgs.version,
		TagName:     cliArgs.version,
		Description: fmt.Sprintf("Version %s of %s", cliArgs.version, tui.ApplicationName),
	}

	paths, err := filepath.Glob(cliArgs.fileglob)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			logrus.ErrorKey: err,
			"fileglob":      cliArgs.fileglob,
		}).Fatal("unable to glob")
	}
	if len(paths) == 0 {
		logrus.WithField("fileglob", cliArgs.fileglob).Fatal("no files matched")
	}
	for _, fpath := range paths {
		fname := path.Base(fpath)
		logrus.WithField("filename", fname).Info("listing")
		link := gitlabLink{
			Name: fname,
			URL:  "https://stuvel.eu/files/beatstripper/" + fname,
		}
		release.Assets.Links = append(release.Assets.Links, link)
	}

	bytes, err := json.MarshalIndent(release, "", "    ")
	if err != nil {
		logrus.WithError(err).Fatal("unable to marshal to JSON")
	}

	return bytes
}

func loadPersonalAccessToken() string {
	fname, err := filepath.Abs(".gitlabAccessToken")
	if err != nil {
		logrus.WithError(err).Fatal("unable to construct absolute path")
	}
	logrus.WithField("filename", fname).Info("reading Gitlab access token")

	tokenBytes, err := ioutil.ReadFile(fname)
	if err != nil {
		logrus.WithError(err).Fatal("unable to read personal access token, see https://gitlab.com/profile/personal_access_tokens")
	}

	return strings.TrimSpace(string(tokenBytes))
}

func doGitlabRequest(payload []byte, authToken string) {
	client := http.Client{
		Timeout: 1 * time.Minute,
	}

	logger := logrus.WithField("url", gitlabURL)
	req, err := http.NewRequest("POST", gitlabURL, bytes.NewReader(payload))
	if err != nil {
		logger.WithError(err).Fatal("unable to create HTTP request")
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Private-Token", authToken)

	resp, err := client.Do(req)
	if err != nil {
		logger.WithError(err).Fatal("error performing HTTP request")
	}

	logger = logger.WithField("statusCode", resp.StatusCode)
	if resp.Header.Get("Content-Type") != "application/json" {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			logger.WithError(err).Fatal("error reading HTTP response")
		}

		logger.WithField("body", string(body)).Fatal("non-JSON response from Gitlab")
	}

	response := anyDict{}
	unmarshaller := json.NewDecoder(resp.Body)
	if err := unmarshaller.Decode(&response); err != nil {
		logger.WithError(err).Fatal("error reading/parsing HTTP response")
	}

	if niceResponse, err := json.MarshalIndent(response, "", "    "); err != nil {
		logger.WithError(err).Error("unable to nicely format response")
		fmt.Printf("%#v\n", response)
	} else {
		logger.Info("Response from Gitlab:")
		os.Stdout.Write(niceResponse)
	}
}

func main() {
	parseCliArgs()

	bytes := makeRequestPayload()
	os.Stdout.Write(bytes)

	token := loadPersonalAccessToken()

	doGitlabRequest(bytes, token)

	os.Stdout.Write([]byte("\n"))
}
