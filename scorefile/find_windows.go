package scorefile

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"path/filepath"

	"github.com/mitchellh/go-homedir"
)

// scorefile returns the score file on Windows.
func scorefile(basename string) (string, error) {
	home, err := homedir.Dir()
	if err != nil {
		return "", err
	}

	scoreFileName := filepath.Join(home, "AppData\\LocalLow\\Hyperbolic Magnetism\\Beat Saber", basename)
	// scoreFileName := filepath.Join(".", basename)
	return scoreFileName, nil
}
