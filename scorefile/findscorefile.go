package scorefile

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"os"
	"path"
)

// Find returns the score file.
func Find(basename string, useLocalFiles bool) (scoreFileName string, err error) {
	if useLocalFiles {
		scoreFileName = basename
	} else {
		scoreFileName, err = scorefile(basename)
		if err != nil {
			return
		}
	}

	_, err = os.Stat(scoreFileName)
	if err != nil {
		return
	}

	scoreFileName = path.Clean(scoreFileName)
	return scoreFileName, nil
}
