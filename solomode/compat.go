package solomode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"errors"
	"fmt"

	"github.com/Masterminds/semver"
	"gitlab.com/dr.sybren/beatstripper/jsoncheck"
)

type dataBag map[string]interface{}

var ErrNotCompatible = errors.New("Single player file is not compatible with this version of BeatStripper")

// SinglePlayerCheckCompatibility parses the given file and converts it back to JSON, then checks whether any data was lost.
func SinglePlayerCheckCompatibility(useLocalFiles, verbose bool) error {
	filename, err := Find(useLocalFiles)
	if err != nil {
		panic(err)
	}
	if verbose {
		fmt.Println(filename)
	}

	parsed, err := ParsePlayerData(filename)
	if err != nil {
		return err
	}

	isCompatible, err := jsoncheck.IsCompatible(filename, parsed, verbose)
	if err != nil {
		return err
	}
	if !isCompatible {
		return ErrNotCompatible
	}
	return nil
}

func VersionNewEnough(playerData PlayerData) bool {
	constraint, err := semver.NewConstraint(fmt.Sprintf(">= %s", playerDataFileVersionSupported))
	if err != nil {
		panic(err)
	}

	version, err := semver.NewVersion(playerData.Version)
	if err != nil {
		// Handle version not being parsable.
		return false
	}

	return constraint.Check(version)
}
