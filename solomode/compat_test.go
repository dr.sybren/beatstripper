package solomode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersionTooOld(t *testing.T) {
	playerData, err := ParsePlayerData(playerDatFilename)
	if err != nil {
		assert.Nil(t, err, "parsing the test file should not cause errors: %s", err.Error())
		return
	}

	assert.True(t, VersionNewEnough(playerData))

	playerData.Version = "9.99.99"
	assert.True(t, VersionNewEnough(playerData))

	playerData.Version = "2.0.14"
	assert.False(t, VersionNewEnough(playerData))

	playerData.Version = "1.99.255"
	assert.False(t, VersionNewEnough(playerData))

	playerData.Version = "0.0.0"
	assert.False(t, VersionNewEnough(playerData))
}
