package solomode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import "fmt"

// UnsupportedVersionError is returned when a JSON file with an unsupported
// version is read or written.
type UnsupportedVersionError struct {
	AttemptedVersion string
	SupportedVersion string
}

func (err UnsupportedVersionError) Error() string {
	return fmt.Sprintf(
		"Data has unsupported version %s; I support %s only",
		err.AttemptedVersion, err.SupportedVersion)
}

// IsUnsupportedVersionError returns true if the given error is an UnsupportedVersionError.
func IsUnsupportedVersionError(err interface{}) bool {
	switch err.(type) {
	case UnsupportedVersionError:
		return true
	default:
		return false
	}
}
