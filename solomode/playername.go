package solomode

import (
	"encoding/json"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

const (
	// BeatSaber uses this as player name when no name has been saved in the file yet.
	unknownPlayerName = "<NO NAME!>"
)

// PlayerName is a string, but empty strings are marshalled as "<NO NAME!>"
type PlayerName string

// UnmarshalJSON just marshals as a regular string, except "<NO NAME!>" maps to the empty string.
func (pn *PlayerName) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	if s == unknownPlayerName {
		*pn = ""
	} else {
		*pn = PlayerName(s)
	}
	return nil
}

// MarshalJSON just marshals as a regular string, except the empty string maps to "<NO NAME!>"
func (pn PlayerName) MarshalJSON() ([]byte, error) {
	if pn == "" {
		return json.Marshal(unknownPlayerName)
	}
	return json.Marshal(string(pn))
}
