package solomode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

type playerNameTestDocument struct {
	Player PlayerName `json:"player"`
}

func TestPlayerNameMarshalEmpty(t *testing.T) {
	document := playerNameTestDocument{}
	bytes, err := json.Marshal(document)
	if err != nil {
		panic(err)
	}
	// The HTML encoding is handled by the encoder, and is not part of the actual
	// code that's being tested here.
	assert.Equal(t, "{\"player\":\"\\u003cNO NAME!\\u003e\"}", string(bytes))
}

func TestPlayerNameMarshalNonempty(t *testing.T) {
	document := playerNameTestDocument{"Test Kees"}
	bytes, err := json.Marshal(document)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, "{\"player\":\"Test Kees\"}", string(bytes))
}

func TestPlayerNameUnmarshalEmpty(t *testing.T) {
	jsonData := []byte("{\"player\":\"<NO NAME!>\"}")
	document := playerNameTestDocument{"unexpected name"}
	err := json.Unmarshal(jsonData, &document)
	if err != nil {
		panic(err)
	}
	assert.EqualValues(t, "", document.Player)
}

func TestPlayerNameUnmarshalNonempty(t *testing.T) {
	jsonData := []byte("{\"player\":\"Test Kees\"}")
	document := playerNameTestDocument{"unexpected name"}
	err := json.Unmarshal(jsonData, &document)
	if err != nil {
		panic(err)
	}
	assert.EqualValues(t, "Test Kees", document.Player)
}
