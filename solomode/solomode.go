package solomode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/dr.sybren/beatstripper/jsoncheck"
)

const (
	// Since PlayerData.dat is versioned, refuse to read any other version than
	// what this code was made for.
	playerDataFileVersionSupported = "2.0.26"

	// The "beatmapCharacteristicName" in PlayerData.dat that is results in
	// nothing being added to the _leaderboardId in LocalLeaderboards.dat.
	standardCharacteristicName = "Standard"
)

const (
	DifficultyEasy       = 0
	DifficultyNormal     = iota
	DifficultyHard       = iota
	DifficultyExpert     = iota
	DifficultyExpertplus = iota
)

var (
	difficultyToString map[int]string = map[int]string{
		DifficultyEasy:       "Easy",
		DifficultyNormal:     "Normal",
		DifficultyHard:       "Hard",
		DifficultyExpert:     "Expert",
		DifficultyExpertplus: "ExpertPlus",
	}
)

// PlayerData represents a PlayerData.dat file.
type PlayerData struct {
	Version      string        `json:"version"`
	LocalPlayers []LocalPlayer `json:"localPlayers"`
	GuestPlayers []GuestPlayer `json:"guestPlayers"`
}

// LocalPlayer contains info about a single local player.
type LocalPlayer struct {
	PlayerID                              string      `json:"playerId"`
	PlayerName                            PlayerName  `json:"playerName"`
	ShouldShowTutorialPrompt              bool        `json:"shouldShowTutorialPrompt"`
	ShouldShow360Warning                  bool        `json:"shouldShow360Warning"`
	AgreedToEula                          bool        `json:"agreedToEula"`
	DidSelectLanguage                     bool        `json:"didSelectLanguage"`
	AgreedToMultiplayerDisclaimer         bool        `json:"agreedToMultiplayerDisclaimer"`
	DidSelectRegionVersion                int64       `json:"didSelectRegionVersion"`
	SelectedAvatarSystemTypeId            string      `json:"selectedAvatarSystemTypeId"`
	PlayerAgreements                      interface{} `json:"playerAgreements"`
	LastSelectedBeatmapDifficulty         int64       `json:"lastSelectedBeatmapDifficulty"`
	LastSelectedBeatmapCharacteristicName string      `json:"lastSelectedBeatmapCharacteristicName"`

	// Most of the sub-structs are not interesting for this application. Keeping
	// them as interface{} means no maintenance required.
	GameplayModifiers           interface{}      `json:"gameplayModifiers"`
	PlayerSpecificSettings      interface{}      `json:"playerSpecificSettings"`
	PracticeSettings            interface{}      `json:"practiceSettings"`
	PlayerAllOverallStatsData   interface{}      `json:"playerAllOverallStatsData"`
	LevelsStatsData             []LevelStatsData `json:"levelsStatsData"`
	MissionsStatsData           interface{}      `json:"missionsStatsData"`
	ShowedMissionHelpIds        interface{}      `json:"showedMissionHelpIds"`
	ColorSchemesSettings        interface{}      `json:"colorSchemesSettings"`
	OverrideEnvironmentSettings interface{}      `json:"overrideEnvironmentSettings"`
	FavoritesLevelIds           interface{}      `json:"favoritesLevelIds"`
	MultiplayerModeSettings     interface{}      `json:"multiplayerModeSettings"`
	CurrentDlcPromoDisplayCount int              `json:"currentDlcPromoDisplayCount"`
	CurrentDlcPromoId           string           `json:"currentDlcPromoId"`
	UserAgeCategory             int              `json:"userAgeCategory":`
	DesiredSensitivityFlag      int              `json:"desiredSensitivityFlag"`
}

// LevelStatsData contains stats on a single level at a single difficulty.
type LevelStatsData struct {
	LevelID                   string `json:"levelId"`
	Difficulty                int    `json:"difficulty"`
	BeatmapCharacteristicName string `json:"beatmapCharacteristicName"`
	HighScore                 int64  `json:"highScore"`
	MaxCombo                  int64  `json:"maxCombo"`
	FullCombo                 bool   `json:"fullCombo"`
	MaxRank                   int64  `json:"maxRank"`
	ValidScore                bool   `json:"validScore"`
	PlayCount                 int64  `json:"playCount"`
}

// GuestPlayer contains info about a single guest player. These are the names
// you can select in party mode when entering the score.
type GuestPlayer struct {
	PlayerName string `json:"playerName"`
}

// ParseAndVerify parses a PlayerData.dat file and verifies its contents can be parsed.
func ParseAndVerify(filename string) (PlayerData, error) {
	playerData, err := ParsePlayerData(filename)
	if err != nil {
		return PlayerData{}, err
	}

	isCompatible, err := jsoncheck.IsCompatible(filename, playerData, false)
	if err != nil {
		return PlayerData{}, err
	}
	if !isCompatible {
		return PlayerData{}, ErrNotCompatible
	}

	if err := checkVersion(playerData); err != nil {
		return PlayerData{}, err
	}

	// Always capitalise the player names before handling the data.
	playerData.CapitaliseGuestPlayers()

	return playerData, nil
}

// ParsePlayerData parses a PlayerData.dat file.
func ParsePlayerData(filename string) (PlayerData, error) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		return PlayerData{}, err
	}
	defer jsonFile.Close()

	fileContents, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return PlayerData{}, err
	}

	var playerData PlayerData
	if err := json.Unmarshal(fileContents, &playerData); err != nil {
		return PlayerData{}, err
	}

	return playerData, nil
}

func checkVersion(playerData PlayerData) error {
	if playerData.Version != playerDataFileVersionSupported {
		err := UnsupportedVersionError{
			AttemptedVersion: playerData.Version,
			SupportedVersion: playerDataFileVersionSupported,
		}
		return err
	}
	return nil
}

// SavePlayerData saves a PlayerData.dat file.
func SavePlayerData(playerData PlayerData, filename string) error {
	// Refuse to write unsupported versions.
	if err := checkVersion(playerData); err != nil {
		return err
	}

	jsonFile, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}

	encoder := json.NewEncoder(jsonFile)
	encoder.SetEscapeHTML(false)

	err = encoder.Encode(playerData)
	closeErr := jsonFile.Close()

	if err != nil {
		os.Remove(filename)
		return err
	}
	return closeErr
}

// LeaderboardID returns the party mode level ID for this song's solo mode score.
func (lsd LevelStatsData) LeaderboardID() string {
	// Determine additions like "90Degree".
	var typeAddition string
	if lsd.BeatmapCharacteristicName != standardCharacteristicName {
		typeAddition = lsd.BeatmapCharacteristicName
	}

	return fmt.Sprintf("%s%s%s",
		lsd.LevelID,
		typeAddition,
		difficultyToString[lsd.Difficulty],
	)
}

func (pd *PlayerData) CapitaliseGuestPlayers() {
	for playerIdx := range pd.GuestPlayers {
		pd.GuestPlayers[playerIdx].PlayerName = strings.Title(pd.GuestPlayers[playerIdx].PlayerName)
	}
}
