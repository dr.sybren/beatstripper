package solomode

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	playerDatFilename = "testdata/PlayerData.dat.json"
	savedDatFilename  = "testdata/savedPlayerData.dat.json"
)

func unmarshal(filename string) map[string]interface{} {
	var data map[string]interface{}

	jsonBytes := readFileContents(filename)
	if err := json.Unmarshal(jsonBytes, &data); err != nil {
		panic(err)
	}

	return data
}

func readFileContents(filename string) []byte {
	fileHandle, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer fileHandle.Close()

	contents, err := ioutil.ReadAll(fileHandle)
	if err != nil {
		panic(err)
	}

	return contents
}

// Test that loading and saving a PlayerData.dat file doesn't change the contents on a semantic level.
// Certain structs will be reordered alphabetically, but should still represent the same data.
func TestSinglePlayerParseSave(t *testing.T) {
	playerData, err := ParsePlayerData(playerDatFilename)
	if err != nil {
		assert.Nil(t, err, "parsing the test file should not cause errors: %s", err.Error())
	}

	err = SavePlayerData(playerData, savedDatFilename)
	defer os.Remove(savedDatFilename)
	assert.Nil(t, err, "saving the test file should not cause errors")

	// Compare file contents after parsing stupidly.
	origContents := unmarshal(playerDatFilename)
	savedContents := unmarshal(savedDatFilename)
	assert.EqualValues(t, origContents, savedContents, "expecting parse+save to be a no-op")
}

func TestLeaderboardID(t *testing.T) {
	lsd := LevelStatsData{
		LevelID:                   "OneStepCloser",
		Difficulty:                3,
		BeatmapCharacteristicName: "Standard",
		HighScore:                 598673,
		MaxCombo:                  491,
		FullCombo:                 true,
		MaxRank:                   5,
		ValidScore:                true,
		PlayCount:                 11,
	}
	assert.EqualValues(t, lsd.LeaderboardID(), "OneStepCloserExpert")

	lsd.Difficulty = 0
	lsd.BeatmapCharacteristicName = "90Degree"
	assert.EqualValues(t, lsd.LeaderboardID(), "OneStepCloser90DegreeEasy")
}

func TestCapitaliseGuestPlayers(t *testing.T) {
	playerData, err := ParsePlayerData(playerDatFilename)
	if err != nil {
		assert.Nil(t, err, "parsing the test file should not cause errors: %s", err.Error())
	}

	expectedOriginalNames := []GuestPlayer{
		{PlayerName: "Sybren"},
		{PlayerName: "met"},
		{PlayerName: "andy"},
		{PlayerName: "Sandra"},
		{PlayerName: "simon"},
		{PlayerName: "double name"},
	}

	assert.EqualValues(t, expectedOriginalNames, playerData.GuestPlayers)
	expectedCapitalisedNames := []GuestPlayer{
		{PlayerName: "Sybren"},
		{PlayerName: "Met"},
		{PlayerName: "Andy"},
		{PlayerName: "Sandra"},
		{PlayerName: "Simon"},
		{PlayerName: "Double Name"},
	}
	playerData.CapitaliseGuestPlayers()
	assert.EqualValues(t, expectedCapitalisedNames, playerData.GuestPlayers)
}
