package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"errors"
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/dr.sybren/beatstripper/partymode"
	"gitlab.com/dr.sybren/beatstripper/solomode"
)

var (
	// ErrNoLocalPlayers is returned when no players are found in PlayerData.dat
	ErrNoLocalPlayers = errors.New("no players in PlayerData.dat")
	// ErrTooManyLocalPlayers is returned when more than one player is found in PlayerData.dat
	ErrTooManyLocalPlayers = errors.New("too many players in PlayerData.dat")
)

const (
	playerNameSoloSuffix = " (solo)"
)

func startupApp() tea.Msg {
	return appStartedMsg{}
}

func findPartyModeScoreFile(useLocalFiles bool) tea.Msg {
	scoreFileName, err := partymode.Find(useLocalFiles)
	if err != nil {
		return errMsg{"unable to find party score file", err}
	}
	return partyModeScoreFileFoundMsg{scoreFileName}
}

func findPlayerDataFile(useLocalFiles bool) tea.Msg {
	scoreFileName, err := solomode.Find(useLocalFiles)
	if err != nil {
		return errMsg{"unable to find player data file", err}
	}
	return playerDataFileFoundMsg{scoreFileName}
}

func parsePartyScoreFile(scoreFileName string) tea.Msg {
	partyModeScores, err := partymode.ParseAndVerify(scoreFileName)
	if err != nil {
		return errMsg{"error parsing score file", err}
	}
	return partyScoreFileParsedMsg{&partyModeScores}
}

func parsePlayerDataFile(scoreFileName string) tea.Msg {
	playerData, err := solomode.ParseAndVerify(scoreFileName)
	if err != nil {
		return errMsg{"error parsing playerdata file", err}
	}
	return playerDataFileParsedMsg{&playerData}
}

func checkPlayerDataFile(playerData *solomode.PlayerData) tea.Msg {
	// For now keep things simple, and only support a single local player.
	switch len(playerData.LocalPlayers) {
	case 0:
		return errMsg{"no local players found", ErrNoLocalPlayers}
	case 1:
		break
	default:
		return errMsg{"too many local players found", ErrTooManyLocalPlayers}
	}

	// Supporting multiple players here is easy, but later it gets a little bit trickier.
	for playerIndex := range playerData.LocalPlayers {
		if playerData.LocalPlayers[playerIndex].PlayerName == "" {
			return playerNameMissingMsg{playerIndex}
		}
	}
	return playerNamesAllKnownMsg{}
}

func skipSoloScores() tea.Msg {
	return allSoloScoresAddedMsg{}
}

func playerNameUpdated(playerIndex int, playerName string) tea.Msg {
	return playerNameGivenMsg{playerIndex, playerName}
}

func storePlayerName(playerData *solomode.PlayerData, playerIndex int, playerName string) tea.Msg {
	playerData.LocalPlayers[playerIndex].PlayerName = solomode.PlayerName(playerName)
	return playerDataFileParsedMsg{playerData}
}

func addSoloScores(playerData *solomode.PlayerData, partyScore *partymode.PartyScore, playerIndex int, songIndex int, playerName string) tea.Msg {
	soloPlayerData := playerData.LocalPlayers[playerIndex]
	soloScores := soloPlayerData.LevelsStatsData
	if songIndex >= len(soloScores) {
		return allSoloScoresAddedMsg{}
	}

	result := partyScore.AddSoloScore(soloPlayerData, songIndex, playerNameSoloSuffix)

	return soloScoreAddedMsg{
		songID:            result.LeaderboardID,
		improvedLastScore: result.BeatenPreviousScore(),
	}
}

func pruneSongScore(partyScore *partymode.PartyScore, songIndex int) tea.Msg {
	if songIndex >= len(partyScore.Leaderboards)-1 {
		return processingDoneMsg{}
	}

	oldboard := partyScore.Leaderboards[songIndex]
	newboard := partymode.PruneScores(oldboard)
	partyScore.Leaderboards[songIndex] = newboard

	return songProcessedMsg{
		songID:        oldboard.LeaderboardID,
		numScoresSeen: len(oldboard.Scores),
		numScoresKept: len(newboard.Scores),
	}
}

func safeRename(fromName, toName string) error {
	bakname := toName + "~"
	if err := os.Rename(toName, bakname); err != nil {
		return err
	}
	if err := os.Rename(fromName, toName); err != nil {
		return err
	}
	return nil
}

func saveScoreFile(scoreFileName string, partyScore partymode.PartyScore) tea.Msg {
	tempname := scoreFileName + ".new"
	if err := partymode.SavePartyMode(partyScore, tempname); err != nil {
		return errMsg{
			fmt.Sprintf("unable to write to %s", tempname),
			err,
		}
	}

	if err := safeRename(tempname, scoreFileName); err != nil {
		return errMsg{
			fmt.Sprintf("unable to rename %s to %s", tempname, scoreFileName),
			err,
		}
	}

	return scoreFileSavedMsg{}
}

func savePlayerDataFile(filename string, playerData *solomode.PlayerData) tea.Msg {
	if playerData == nil {
		return playerDataFileSavedMsg{}
	}

	tempname := filename + ".new"
	if err := solomode.SavePlayerData(*playerData, tempname); err != nil {
		return errMsg{
			fmt.Sprintf("unable to write to %s", tempname),
			err,
		}
	}

	if err := safeRename(tempname, filename); err != nil {
		return errMsg{
			fmt.Sprintf("unable to rename %s to %s", tempname, filename),
			err,
		}
	}

	return playerDataFileSavedMsg{}
}

func skipSavingFiles() tea.Msg {
	return playerDataSavingSkippedMsg{}
}

func skipSavingPartyScore() tea.Msg {
	return partyScoreSavingSkippedMsg{}
}
