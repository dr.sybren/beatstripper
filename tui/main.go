package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	tea "github.com/charmbracelet/bubbletea"
	"github.com/sirupsen/logrus"
)

// ProgramOptions can be passed from the CLI to influence how the program behaves.
type ProgramOptions struct {
	// When true, add scores from solo mode to the party leaderboard.
	AddSolo bool

	// Whether to find files in the current working directory (true), or in the
	// actual game data directory (false).
	UseLocalFiles bool

	// When true, skip saving of files.
	DryRun bool
}

// Main runs the TUI application.
func Main(options ProgramOptions) {
	program := tea.NewProgram(NewModel(options))

	program.EnterAltScreen()
	err := program.Start()
	program.ExitAltScreen()

	if err != nil {
		logrus.WithError(err).Fatal("alas, there's been an error")
	}
}
