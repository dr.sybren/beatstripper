package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"gitlab.com/dr.sybren/beatstripper/partymode"
	"gitlab.com/dr.sybren/beatstripper/solomode"
)

type appStartedMsg struct{}

type partyModeScoreFileFoundMsg struct {
	filename string
}

type playerDataFileFoundMsg struct {
	filename string
}

type partyScoreFileParsedMsg struct {
	partyscore *partymode.PartyScore
}

type playerDataFileParsedMsg struct {
	playerData *solomode.PlayerData
}

type playerNameMissingMsg struct {
	localPlayerIndex int
}
type playerNameGivenMsg struct {
	playerIndex int
	playerName  string
}

type playerNamesAllKnownMsg struct{}
type soloScoreAddedMsg struct {
	songID            string
	improvedLastScore bool
}
type allSoloScoresAddedMsg struct{}

type songProcessedMsg struct {
	songID        string
	numScoresSeen int
	numScoresKept int
}

type playerDataSavingSkippedMsg struct{}
type partyScoreSavingSkippedMsg struct{}
type processingDoneMsg struct{}

type playerDataFileSavedMsg struct{}
type scoreFileSavedMsg struct{}

type errMsg struct {
	message string
	err     error
}

// For messages that contain errors it's often handy to also implement the
// error interface on the message.
func (e errMsg) Error() string { return e.err.Error() }
