package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"strings"

	"github.com/charmbracelet/bubbles/progress"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/beatstripper/partymode"
	"gitlab.com/dr.sybren/beatstripper/solomode"
)

type programState string

const (
	psStarting          programState = "STARTING"
	psLoadingFiles      programState = "LOADING_FILES"
	psAskingNames       programState = "ASKING_NAMES"
	psMergingSoloScores programState = "MERGING_SOLO_SCORES"
	psPruningScores     programState = "PRUNING_SCORES"
	psSavingFiles       programState = "SAVING_FILES"
	psDone              programState = "DONE"
)

type model struct {
	state   programState
	options ProgramOptions

	progress          *progress.Model
	updatesBeforeWait int

	songIndex int
	done      bool

	err        error
	errMessage string

	lastAction string

	partyScoreFileName string
	partyScore         *partymode.PartyScore
	numScoresSeen      int
	numScoresKept      int

	// Solo mode
	playerDataFileName  string
	playerData          *solomode.PlayerData
	numSoloScoresBeaten int

	playerIndex int
	nameInput   textinput.Model
}

// NewModel returns a new TUI model.
func NewModel(options ProgramOptions) tea.Model {
	progress, err := progress.NewModel(progress.WithScaledGradient("#e52b2b", "#2b37e5"))
	if err != nil {
		logrus.WithError(err).Fatal("error not initializing progress model")
	}

	return model{
		state:     psStarting,
		options:   options,
		progress:  progress,
		nameInput: textinput.NewModel(),
	}
}

func (m model) PlayerName() string {
	return strings.TrimSpace(m.nameInput.Value())
}

func (m model) AnyScoreChanged() bool {
	return m.numScoresSeen != m.numScoresKept || m.numSoloScoresBeaten > 0
}
