package tui

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

func TestAnyDataChanged(t *testing.T) {
	teaModel := NewModel(ProgramOptions{})
	m, ok := teaModel.(model)
	assert.True(t, ok)
	assert.False(t, m.AnyScoreChanged())

	m.numScoresSeen = 431
	m.numScoresKept = 431
	m.numSoloScoresBeaten = 0
	assert.False(t, m.AnyScoreChanged())

	m.numScoresSeen = 431
	m.numScoresKept = 411
	m.numSoloScoresBeaten = 0
	assert.True(t, m.AnyScoreChanged())

	m.numScoresSeen = 431
	m.numScoresKept = 431
	m.numSoloScoresBeaten = 4
	assert.True(t, m.AnyScoreChanged())
}
