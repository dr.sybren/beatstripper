package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"
	"time"

	tea "github.com/charmbracelet/bubbletea"
)

const (
	progressPadding = 2

	// Artificially slow down the program so you can actually see the progress bar progressing.
	waitEvery = 3
)

func (m model) Init() tea.Cmd {
	return startupApp
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	m.maybeWait()

	// Some messages always need to be processed, regardless of state.
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		}

	case tea.WindowSizeMsg:
		m.progress.Width = msg.Width - progressPadding*2 - 4
		return m, nil

	case errMsg:
		m.err = msg.err
		m.errMessage = msg.message
		return m, nil
	}

	// Call state-dependent model updates, if available.
	switch m.state {
	case psAskingNames:
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "enter":
				if m.PlayerName() == "" {
					return m, skipSoloScores
				}
				return m, func() tea.Msg {
					return storePlayerName(m.playerData, m.playerIndex, m.PlayerName())
				}
			}

			var cmd tea.Cmd
			m.nameInput.Focus()
			m.nameInput, cmd = m.nameInput.Update(msg)
			return m, cmd
		}
	}

	// Regular update handling.
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "q", "enter", " ":
			return m, tea.Quit
		default:
			return m, nil
		}

	case appStartedMsg:
		m.state = psLoadingFiles
		return m, func() tea.Msg {
			return findPartyModeScoreFile(m.options.UseLocalFiles)
		}

	case partyModeScoreFileFoundMsg:
		m.lastAction = "Score file found"
		m.partyScoreFileName = msg.filename
		return m, func() tea.Msg {
			return parsePartyScoreFile(m.partyScoreFileName)
		}

	case partyScoreFileParsedMsg:
		m.state = psLoadingFiles
		m.lastAction = "Score file parsed"
		m.partyScore = msg.partyscore
		m.songIndex = 0
		return m, func() tea.Msg {
			return findPlayerDataFile(m.options.UseLocalFiles)
		}

	case playerDataFileFoundMsg:
		m.lastAction = "Player data file found"
		m.playerDataFileName = msg.filename
		return m, func() tea.Msg {
			return parsePlayerDataFile(m.playerDataFileName)
		}

	case playerDataFileParsedMsg:
		m.lastAction = "Player file parsed"
		m.playerData = msg.playerData

		if m.options.AddSolo {
			return m, func() tea.Msg {
				return checkPlayerDataFile(m.playerData)
			}
		}
		return m, skipSoloScores

	case playerNameMissingMsg:
		m.state = psAskingNames
		m.lastAction = fmt.Sprintf("Player #%d name missing", msg.localPlayerIndex)
		return m, nil

	case playerNamesAllKnownMsg:
		m.state = psMergingSoloScores
		m.lastAction = fmt.Sprintf("All player names known")
		m.songIndex = 0

		return m, func() tea.Msg {
			return addSoloScores(m.playerData, m.partyScore, 0, m.songIndex, m.PlayerName())
		}

	case soloScoreAddedMsg:
		m.lastAction = fmt.Sprintf("Added solo score to %s", msg.songID)
		m.songIndex++
		if msg.improvedLastScore {
			m.numSoloScoresBeaten++
		}
		return m, func() tea.Msg {
			return addSoloScores(m.playerData, m.partyScore, 0, m.songIndex, m.PlayerName())
		}

	case allSoloScoresAddedMsg:
		m.state = psPruningScores
		m.lastAction = fmt.Sprintf("Solo scores added to party mode")
		m.songIndex = 0
		return m, func() tea.Msg {
			return pruneSongScore(m.partyScore, m.songIndex)
		}

	case songProcessedMsg:
		m.state = psPruningScores
		m.lastAction = fmt.Sprintf("Pruned %s", msg.songID)
		m.songIndex++
		m.numScoresSeen += msg.numScoresSeen
		m.numScoresKept += msg.numScoresKept
		return m, func() tea.Msg {
			return pruneSongScore(m.partyScore, m.songIndex)
		}

	case processingDoneMsg:
		m.lastAction = "Finished pruning songs"

		if m.options.DryRun {
			return m, skipSavingFiles
		}

		m.state = psSavingFiles
		if !m.AnyScoreChanged() {
			return m, skipSavingPartyScore
		}

		return m, func() tea.Msg {
			return saveScoreFile(m.partyScoreFileName, *m.partyScore)
		}

	case scoreFileSavedMsg, partyScoreSavingSkippedMsg:
		m.lastAction = "Score file saved"
		return m, func() tea.Msg {
			return savePlayerDataFile(m.playerDataFileName, m.playerData)
		}

	case playerDataFileSavedMsg:
		m.state = psDone
		m.lastAction = "All files saved"
		m.done = true
		return m, nil

	case playerDataSavingSkippedMsg:
		m.state = psDone
		m.lastAction = "Dry run: skipped saving"
		m.done = true
		return m, nil
	}

	return m, nil
}

func (m *model) maybeWait() {
	if m.updatesBeforeWait > 0 {
		m.updatesBeforeWait--
		return
	}
	m.updatesBeforeWait = waitEvery

	// This causes BubbleTea to get time to redraw, so the exact time to wait
	// doesn't matter much, because the lower bound is set by the redraw time.
	<-time.After(1 * time.Nanosecond)
}
