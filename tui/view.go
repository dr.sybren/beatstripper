package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatStripper.
 *
 * BeatStripper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatStripper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatStripper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"
	"strings"

	"github.com/muesli/termenv"
)

var (
	colorProfile     = termenv.ColorProfile()
	styleBasic       = termenv.Style{}
	styleHeader      = styleBasic.Foreground(colorProfile.Color("#e5882b")).Bold()
	styleRunning     = styleBasic.Foreground(colorProfile.Color("#e5e52b"))
	styleError       = styleBasic.Foreground(colorProfile.Color("#e52b2b")).Bold()
	styleDone        = styleBasic.Foreground(colorProfile.Color("#2be52b")).Bold()
	styleEnter       = styleBasic.Bold()
	styleDimmed      = styleBasic.Foreground(colorProfile.Color("#444444"))
	styleAskingInput = styleBasic.Foreground(colorProfile.Color("#e5e52b")).Bold()
)

func (m model) View() string {
	header := fmt.Sprintf("%s %s", ApplicationName, ApplicationVersion)
	s := styleHeader.Styled(header)
	s += ": "
	switch {
	case m.err != nil:
		s += styleError.Styled("ERROR")
	case m.done:
		s += styleDone.Styled("done")
	default:
		s += styleRunning.Styled("running")
	}
	s += "\n\n"

	s += fmt.Sprintf("       State: %s\n", m.state)
	s += fmt.Sprintf(" Last action: %s\n", m.lastAction)

	switch m.state {
	case psAskingNames:
		s += styleAskingInput.Styled(fmt.Sprintf(" Player Name: %s", m.nameInput.View()))
		s += "\n\n"
	case psMergingSoloScores, psPruningScores, psSavingFiles, psDone:
		s += "\n"
		if m.partyScoreFileName != "" {
			s += fmt.Sprintf(" Party File : %s\n", m.partyScoreFileName)
		}
		s += fmt.Sprintf(" Scores seen: %d\n", m.numScoresSeen)
		s += fmt.Sprintf("        kept: %d\n", m.numScoresKept)
		s += fmt.Sprintf("     removed: %d\n\n", m.numScoresSeen-m.numScoresKept)
	}

	if m.playerData != nil {
		s += fmt.Sprintf("Solo players : %d\n", len(m.playerData.LocalPlayers))
		s += fmt.Sprintf("Scores Beaten: %d\n", m.numSoloScoresBeaten)
	}
	if m.playerDataFileName != "" {
		s += fmt.Sprintf(" Player File: %s\n", m.playerDataFileName)
	}

	if m.err != nil {
		styledError := styleError.Styled(fmt.Sprintf("ERROR: %s; %s", m.errMessage, m.err.Error()))
		s += fmt.Sprintf("\n %s", styledError)
	} else if m.partyScore != nil {
		s += m.progressbar()
	}

	var enterMsg string
	if m.err != nil || m.done {
		enterMsg = styleEnter.Styled("Press ENTER to close.")
	} else {
		enterMsg = styleDimmed.Styled("Press CTRL+C to close.")
	}
	s += "\n\n\n" + enterMsg + "\n"

	return s
}

func (m model) progressPercent() (percent float64) {
	if m.done {
		return 1.0
	}

	switch m.state {
	case psStarting, psLoadingFiles, psAskingNames:
		return 0.0
	case psSavingFiles, psDone:
		return 1.0

	case psMergingSoloScores:
		localPlayers := m.playerData.LocalPlayers
		if len(localPlayers) == 0 {
			return 0.0
		}
		divisor := len(localPlayers[m.playerIndex].LevelsStatsData)
		if divisor == 0 {
			return 0.0
		}
		percent = float64(m.songIndex) / float64(divisor)
		percent = percent / 2 // first 50%

	case psPruningScores:
		divisor := len(m.partyScore.Leaderboards)
		if divisor == 0 {
			return 0.0
		}
		percent = float64(m.songIndex) / float64(divisor)
		if m.options.AddSolo {
			percent = percent/2 + 0.5 // second 50%
		}
	}

	if percent > 1.0 {
		percent = 1.0
	}
	return
}

func (m model) progressbar() string {
	pad := strings.Repeat(" ", progressPadding)
	return pad + m.progress.View(m.progressPercent())
}
